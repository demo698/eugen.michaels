
export class InfoPanelConfig {
  expanded? = false;
  icon: string;
  prefix: string;
  items: number[];
}