import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { TranslateService } from "@ngx-translate/core";
import { BehaviorSubject, Observable } from "rxjs";
import { first, tap } from 'rxjs/operators';
import { LoggerService } from "./logger.service";

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  private _lang = 'en';

  public langs: string[] = ['en', 'de', 'ru'];

  public isMenuOpen$ = new BehaviorSubject<boolean>(false); 

  public isBannerVisible$ = new BehaviorSubject<boolean>(true);;

  constructor(
    private readonly http: HttpClient,
    private readonly matIconRegistry: MatIconRegistry,
    private readonly domSanitizer: DomSanitizer,
    private readonly translate: TranslateService,
    private readonly loggerService: LoggerService) {
      this.initTranslate() ;
    }

  public initialize(): Observable<any> {
    return this.http.get<any>(`../assets/images/import.json`)
      .pipe(first(),tap(
        data => {
          for (const icon of Object.keys(data)) {
            this.matIconRegistry.addSvgIcon(
              icon,
              this.domSanitizer.bypassSecurityTrustResourceUrl(`../assets/images/${data[icon]}`)
            );
          }
          this.loggerService.debug(this, 'Extended svg-icons are loaded');
        }
      ));
  }

    /*--------------------------Translation-------------------------------------------------------*/
    private initTranslate(): void {
      this.translate.setDefaultLang('en');
      this._lang = localStorage.getItem('lang') || 'en';
      this.translate.use(this._lang);
      this.loggerService.debug(this, 'ngx-translate framework is initilised');
    }
  
    set lang(value: string) {
      localStorage.setItem('lang', value);
      window.location.reload();
    }
  
    get lang(): string {
      return this._lang;
    }
}