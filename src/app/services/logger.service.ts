import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  constructor() { }

  public debug( sender: object, ...args: any[] ): void {
    if ( environment.showLogs ) {
      console.log('DEBUG: ', [sender?.constructor?.name].concat(args) );
    }
  }

  public error( sender: object, ...args: any[] ): void {
    console.error('ERROR:', [sender?.constructor?.name].concat(args));
  }
}