import { Component } from '@angular/core';

@Component({
  selector: 'app-about-site',
  templateUrl: './about-site.component.html',
  styleUrls: ['./about-site.component.scss']
})
export class AboutSiteComponent {
  public items = [
    {
      title: 'Angular',
      descr: 'about-site.item1.descr',
      text:  'about-site.item1.text',
      image: './assets/images/angular.svg'
    },
    {
      title: 'Gitlab',
      descr: 'about-site.item2.descr',
      text:  'about-site.item2.text',
      image: './assets/images/gitlab.svg'
    },
    {
      title: 'Angular Material',
      descr: 'about-site.item3.descr',
      text:  'about-site.item3.text',
      image: './assets/images/material.svg'
    },
    {
      title: 'AWS',
      descr: 'about-site.item4.descr',
      text:  'about-site.item4.text',
      image: './assets/images/aws.svg'
    }
  ]
}
