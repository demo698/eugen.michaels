import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from 'src/app/models';

@Component({
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent {

  public isSuccess: boolean;

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.isSuccess = data.isSuccess;
  }
}
