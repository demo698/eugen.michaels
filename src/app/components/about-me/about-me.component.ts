import { Component } from '@angular/core';
import { InfoPanelConfig } from 'src/app/models';

@Component({
  selector: 'app-about-me',
  templateUrl: './about-me.component.html',
  styleUrls: ['./about-me.component.scss']
})
export class AboutMeComponent {
  configs: InfoPanelConfig[] = [
    {
      icon: 'cast_for_education',
      expanded: true,
      prefix: 'about-me.education.',
      items: [1,2,3]
    },
    {
      icon: 'article',
      prefix: 'about-me.publications.',
      items: [1,2,3,4]
    },
    {
      icon: 'work',
      prefix: 'about-me.work.',
      items: [1,2,3,4,5,6,7,8,9]
    },
    {
      icon: 'language',
      prefix: 'about-me.languages.',
      items: [1,2,3]
    },
    {
      icon: 'computer',
      prefix: 'about-me.skills.',
      items: [1,2,3,4,5,6,7,8]
    }
  ];
}
