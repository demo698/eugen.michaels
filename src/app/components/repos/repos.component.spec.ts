import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { TranslateFakeLoader, TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { AppModule } from 'src/app/app.module';
import { MaterialModule } from 'src/app/material.module';
import { ReposComponent } from './repos.component';

describe('ReposComponent', () => {
  let component: ReposComponent;
  let fixture: ComponentFixture<ReposComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReposComponent ],
      imports: [  
        AppModule,    
        MaterialModule, 
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useClass: TranslateFakeLoader
          }
      })]
    })
    .compileComponents();
  });

  beforeEach(inject([MatIconRegistry, DomSanitizer], (mir: MatIconRegistry, sanitizer: DomSanitizer) => {
    const sanitizedUrl = sanitizer.bypassSecurityTrustResourceUrl('/fake/icon.svg');
    mir.addSvgIcon("c-sharp", sanitizedUrl);
    mir.addSvgIcon("angular", sanitizedUrl);
    mir.addSvgIcon("go", sanitizedUrl);

    fixture = TestBed.createComponent(ReposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
