import { Component } from '@angular/core';
import { InfoPanelConfig } from 'src/app/models';

@Component({
  selector: 'app-repos',
  templateUrl: './repos.component.html',
  styleUrls: ['./repos.component.scss']
})
export class ReposComponent {
  public repos = [
    {
      title: 'Angular',
      descr: 'repos.item1.descr',
      text:  'repos.item1.text',
      image: './assets/images/angular.svg',
      link:  ''
    },
  ];

  public configEducation: InfoPanelConfig = {
    icon: 'cast_for_education',
    expanded: true,
    prefix: 'about-me.education.',
    items: [1,2,3]
  };
}
