import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigService } from '../../services/config.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  private _link: string[] = [];

  public navItems: any[] = [
    {
      title: 'about-me',
      icon: 'waving_hand',
      link: '/about-me'
    },
    {
      title: 'repos',
      icon: 'code',
      link: '/repos'
    },
    {
      title: 'about-site',
      icon: 'web',
      link: '/about-site'
    },
    {
      title: 'contacts',
      icon: 'contacts',
      link: '/contacts'
    }
  ];

  public showFiller = false;
  
  constructor(
    private readonly router: Router,
    private readonly configService: ConfigService) {
      
  }
  ngOnInit(): void {
    this.link = [sessionStorage.getItem('link') || '/about-me'];
  }

  get link(): string[] {
    return this._link;
  }

  set link(value: string[]) {
    this._link = value;
    sessionStorage.setItem('link', value[0]);
    this.router.navigate(value);
    this.configService.isMenuOpen$.next(false);
  }
}
