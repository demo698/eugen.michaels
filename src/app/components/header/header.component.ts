import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/services/config.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public langs: string[] = [];
  public lang = 'en';

  constructor(
    private readonly configService: ConfigService) { 
  }

  ngOnInit(): void {
    this.langs = this.configService.langs;
    this.lang = this.configService.lang;
  }

  public setLang(lang: string): void {
    this.configService.lang = lang;
  }

  public langTitle(lang: string): string {
    switch (lang) {
      case 'en':
        return 'English';
      case 'de':
        return 'Deutsch';
      case 'ru': 
        return 'Русский';
      default:
        return lang;
    }
  }

  public toggleManu(): void {
    const isMenuOpen = this.configService.isMenuOpen$.getValue();
    this.configService.isMenuOpen$.next(!isMenuOpen);
  }
}
