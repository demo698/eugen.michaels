import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppModule } from '../../app.module';

import { HeaderComponent } from './header.component';
import { MaterialModule } from 'src/app/material.module';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        HttpClientTestingModule,
        AppModule
      ],
      declarations: [ HeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(inject([MatIconRegistry, DomSanitizer], (mir: MatIconRegistry, sanitizer: DomSanitizer) => {
    const sanitizedUrl = sanitizer.bypassSecurityTrustResourceUrl('/fake/icon.svg');
    mir.addSvgIcon("flag-en", sanitizedUrl);
    mir.addSvgIcon("flag-de", sanitizedUrl);
    mir.addSvgIcon("flag-ru", sanitizedUrl);

    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
