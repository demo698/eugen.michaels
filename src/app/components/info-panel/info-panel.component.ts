import { Component, Input } from '@angular/core';
import { InfoPanelConfig } from '../../models/info-panel-config';

@Component({
  selector: 'app-info-panel',
  templateUrl: './info-panel.component.html',
  styleUrls: ['./info-panel.component.scss']
})
export class InfoPanelComponent {
  @Input() public config = new InfoPanelConfig();
}
