import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DialogComponent } from '../dialog/dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent {

  public isLoading = false;
  
  public emailControl = new FormControl('', [Validators.required, Validators.email]);
  public subjectControl = new FormControl('', [Validators.required]);
  public messageControl = new FormControl('', [Validators.required]);
  public form = new FormGroup({
    "email": this.emailControl,
    "subject": this.subjectControl,
    "message": this.messageControl
  });

  constructor( private readonly http: HttpClient,
               public dialog: MatDialog) {
  }


  public sendMessage() {
    this.isLoading = true;
    const url = 'https://64mogqxguf.execute-api.eu-central-1.amazonaws.com/test/email';
    const email = this.form.get('email')?.value;
    const subject = this.form.get('subject')?.value;
    const message = this.form.get('message')?.value;
    const body = {
      subject: 'Message from www.eugen-michaels.de',
      text: `${email}\n${subject}\n${message}`
    }
    this.http.post(url, body).subscribe({
      next: _ => {
        this.form.reset();
        this.dialog.open(DialogComponent, {data: {isSuccess: true}});
        this.isLoading = false;
      },
      error: _ => {
        this.dialog.open(DialogComponent, {data: {isSuccess: false}});
        this.isLoading = false;
      }
    });
  }
}
