import { Component } from '@angular/core';
import { ConfigService } from 'src/app/services/config.service';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent {

  constructor(
    private readonly configService: ConfigService) { 
  }

  public closeBanner() {
    this.configService.isBannerVisible$.next(false);
  }
}
