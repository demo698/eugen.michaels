import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutMeComponent, AboutSiteComponent, ContactsComponent, 
         ReposComponent } from './components';

const routes: Routes = [
  { path: 'about-me',     component: AboutMeComponent },
  { path: 'repos',        component: ReposComponent },
  { path: 'about-site',   component: AboutSiteComponent },
  { path: 'contacts',     component: ContactsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
