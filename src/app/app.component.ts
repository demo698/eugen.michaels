import { Component } from '@angular/core';
import { ConfigService } from './services/config.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public isMenuOpen$ = this.configService.isMenuOpen$;
  public isBannerVisible$ = this.configService.isBannerVisible$;
  
  constructor(private readonly configService: ConfigService) {
  }
}
